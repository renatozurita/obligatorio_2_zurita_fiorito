package interfaz;

import dominio.Estadistica;
import dominio.Pregunta;
import dominio.Sistema;
import java.awt.Color;
import java.awt.GridLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.TimerTask;
import javax.swing.*;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class Juego extends javax.swing.JFrame {
    
    private Sistema modelo;
    private Estadistica estadistica;
    java.util.Timer timer = new java.util.Timer();
    ButtonGroup bg = new ButtonGroup();
    private int cantPreguntas;
    private int contadorPreg = 1;
    private int tiempo;
    private int tipoPregunta;
    private int numeroPregunta;
    private JButton[] botones;
    private boolean start;
    DefaultListModel modeloLista = new DefaultListModel();
    Pregunta p;
    
    public Juego(Sistema unSistema, Estadistica unaEstadistica) throws IOException {
        initComponents();
        this.setModelo(unSistema);
        this.setEstadistica(unaEstadistica);
        bg.add(jRadioButtonArriba);
        bg.add(jRadioButtonAbajo);
        bg.clearSelection();
        jRadioButtonArriba.setVisible(false);
        jRadioButtonAbajo.setVisible(false);
        jTextAreaPregunta.setVisible(false);
        jListRptas.setModel(modeloLista);
        jListRptas.removeAll();
        jListRptas.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jListRptas.setVisible(false);
        jLabelComentarios.setVisible(false);
        jPanel1.setLayout(new GridLayout(1,5));
        numeroPregunta=0;
        botones = new JButton[modelo.getQuiz().getCantidadPreguntas()];
        for (int i = 0; i < modelo.getQuiz().getCantidadPreguntas(); i++) {
            JButton boton = new JButton();
            jPanel1.add(boton);
            botones[i]=boton;
        }
    }
    
    public void cargarLista(Pregunta p) {
        jListRptas.setVisible(true);
        modeloLista.removeAllElements();
        for (int i = 0; i < p.getRespuestas().size(); i++) {
            modeloLista.addElement(p.getRespuestas().get(i).substring(1));
        }
    }
    
    public Sistema getModelo() {
        return modelo;
    }
    
    public void setModelo(Sistema unModelo) {
        this.modelo = unModelo;
    }
    public void setEstadistica(Estadistica unaEstadistica){
        this.estadistica = unaEstadistica;
    }
    
    public void reproducirSonido(String rutaSonido){
        InputStream sonido;
        try{
            sonido=new FileInputStream(new File(rutaSonido));
            AudioStream audios = new AudioStream(sonido);
            AudioPlayer.player.start(audios);
        }catch(Exception e){
            System.out.println("dou");
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        barraProgreso = new javax.swing.JProgressBar();
        jLabel1 = new javax.swing.JLabel();
        jLabelPregNum = new javax.swing.JLabel();
        jLabelPregunta = new javax.swing.JLabel();
        jLabelRespuestas = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListRptas = new javax.swing.JList<>();
        jButtonSIguiente = new javax.swing.JButton();
        jButtonComenzar = new javax.swing.JButton();
        jLabelTiempo = new javax.swing.JLabel();
        jButtonSalir = new javax.swing.JButton();
        jRadioButtonArriba = new javax.swing.JRadioButton();
        jRadioButtonAbajo = new javax.swing.JRadioButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaPregunta = new javax.swing.JTextArea();
        jLabelGif = new javax.swing.JLabel();
        jLabelComentarios = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jLabel1.setText("Porcentaje de correctas:");

        jLabelPregNum.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelPregNum.setText("Pregunta numero: ");

        jLabelPregunta.setBackground(new java.awt.Color(255, 255, 255));
        jLabelPregunta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelPregunta.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabelRespuestas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelRespuestas.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jListRptas.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jListRptas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListRptasMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jListRptas);

        jButtonSIguiente.setText("Siguiente");
        jButtonSIguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSIguienteActionPerformed(evt);
            }
        });

        jButtonComenzar.setText("Comenzar!");
        jButtonComenzar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonComenzarActionPerformed(evt);
            }
        });

        jLabelTiempo.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jLabelTiempo.setText("Tiempo restante:");

        jButtonSalir.setText("Salir");
        jButtonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalirActionPerformed(evt);
            }
        });

        jRadioButtonArriba.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jRadioButtonArriba.setText("True");

        jRadioButtonAbajo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jRadioButtonAbajo.setSelected(true);
        jRadioButtonAbajo.setText("False");

        jTextAreaPregunta.setColumns(20);
        jTextAreaPregunta.setRows(5);
        jScrollPane2.setViewportView(jTextAreaPregunta);

        jLabelComentarios.setText("Comentarios");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(barraProgreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelPregunta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabelRespuestas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                            .addComponent(jRadioButtonArriba)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 435, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                    .addComponent(jButtonSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(79, 79, 79)
                                                    .addComponent(jButtonComenzar))
                                                .addComponent(jLabelPregNum, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGap(0, 0, Short.MAX_VALUE)))
                                    .addGap(2, 2, 2)))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jButtonSIguiente))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(47, 47, 47)
                            .addComponent(jLabelTiempo, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(jRadioButtonAbajo)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 632, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabelGif, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelComentarios)
                        .addGap(145, 145, 145))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelTiempo, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(barraProgreso, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelPregNum, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelPregunta, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelRespuestas, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioButtonArriba)
                                        .addGap(22, 22, 22)
                                        .addComponent(jRadioButtonAbajo))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelGif, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelComentarios)))
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonComenzar)
                    .addComponent(jButtonSIguiente)
                    .addComponent(jButtonSalir))
                .addGap(50, 50, 50))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void mostrarComentario(String comentario, Color color){
        jLabelComentarios.setVisible(true);
        jLabelComentarios.setForeground(color);
        jLabelComentarios.setText(comentario);
    }
    
    private void mostrarGif(boolean Bien){
        jLabelGif.setVisible(true);
        Random ran = new Random();
        int numeroRandom = ran.nextInt((5-1)+1)+1;
        if (Bien) {
            jLabelGif.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bien"+numeroRandom+".gif")));
            botones[0].setBorder(BorderFactory.createLineBorder(Color.red, 100));
            System.out.println(botones[0].getBorder());
        }else{
            jLabelGif.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mal"+numeroRandom+".gif")));
        }
    }
    
    private void pintarCasillero(int casillero,Color color){
        botones[casillero].setBorder(BorderFactory.createLineBorder(color, 50));
    }
    
    private void jButtonSIguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSIguienteActionPerformed
        
        switch (tipoPregunta) {
            case 1:
                if (!bg.getSelection().isSelected()) {
                    mostrarComentario("No seleccionaste ninguna respuesta.",Color.yellow);
                } else {
                    if (jRadioButtonArriba.isSelected()) {
                        if (jRadioButtonArriba.getText().equals(p.correcta(p).substring(1))) {
                            barraProgreso.setValue(barraProgreso.getValue() + 100 / cantPreguntas);
                            botones[0].setForeground(Color.green);
                            reproducirSonido("./src/audios/correcto.wav");
                            mostrarGif(true);
                            mostrarComentario("¡Correcto!",Color.green);
                            pintarCasillero(numeroPregunta, Color.green);
                            numeroPregunta++;
                        } else {
                            reproducirSonido("./src/audios/incorrecto.wav");
                            mostrarGif(false);
                            mostrarComentario("Incorrecto :(",Color.red);
                            pintarCasillero(numeroPregunta, Color.red);
                            numeroPregunta++;
                        }
                        estadistica.getRespuestasUsuario().add(jRadioButtonArriba.getText());
                        estadistica.getTiempos().add(p.getTiempo()-tiempo);
                    } else {
                        if (jRadioButtonAbajo.getText().equals(p.correcta(p).substring(1))) {
                            barraProgreso.setValue(barraProgreso.getValue() + 100 / cantPreguntas);
                            mostrarGif(true);
                            reproducirSonido("./src/audios/correcto.wav");
                            mostrarComentario("¡Correcto!",Color.green);
                            pintarCasillero(numeroPregunta, Color.green);
                            numeroPregunta++;
                        } else {
                            mostrarGif(false);
                            reproducirSonido("./src/audios/incorrecto.wav");
                            mostrarComentario("Incorrecto :(",Color.red);
                            pintarCasillero(numeroPregunta, Color.red);
                            numeroPregunta++;
                        }
                        estadistica.getRespuestasUsuario().add(jRadioButtonAbajo.getText());
                        estadistica.getTiempos().add(p.getTiempo()-tiempo);
                    }
                    
                    
                }
                break;
            case 2:
                if (jListRptas.getSelectedIndex() < 0) {
                    mostrarComentario("No seleccionaste ninguna respuesta",Color.yellow);
                } else {
                    if (jListRptas.getSelectedValue().equals(p.correcta(p).substring(1))) {
                        mostrarGif(true);
                        reproducirSonido("./src/audios/correcto.wav");
                        barraProgreso.setValue(barraProgreso.getValue() + 100 / cantPreguntas);
                        mostrarComentario("¡Correcto!",Color.green);
                        pintarCasillero(numeroPregunta, Color.green);
                        numeroPregunta++;
                    } else {
                        mostrarGif(false);
                        reproducirSonido("./src/audios/incorrecto.wav");
                        mostrarComentario("Incorrecto :(",Color.red);
                        pintarCasillero(numeroPregunta, Color.red);
                        numeroPregunta++;
                    }
                    estadistica.getRespuestasUsuario().add(jListRptas.getSelectedValue());
                    estadistica.getTiempos().add(p.getTiempo()-tiempo);
                }
                break;
            
            case 3:
                if (p.getRespuestas().get(0).contains(jTextAreaPregunta.getText())
                        || jTextAreaPregunta.getText().contains(p.getRespuestas().get(0))) {
                    barraProgreso.setValue(barraProgreso.getValue() + 100 / cantPreguntas);
                    mostrarGif(true);
                    reproducirSonido("./src/audios/correcto.wav");
                    mostrarComentario("¡Correcto!",Color.green);
                    pintarCasillero(numeroPregunta, Color.green);
                    numeroPregunta++;
                    
                } else {
                    mostrarGif(false);
                    reproducirSonido("./src/audios/incorrecto.wav");
                    mostrarComentario("Incorrecto :(",Color.red);
                    pintarCasillero(numeroPregunta, Color.red);
                    numeroPregunta++;
                }
                estadistica.getRespuestasUsuario().add(jTextAreaPregunta.getText());
                estadistica.getTiempos().add(p.getTiempo()-tiempo);
                break;
            default:
                break;
        }
        contadorPreg++;
        if (contadorPreg - 1 < modelo.getQuiz().getPreguntas().size()) {
            p = modelo.proximaPregunta(p);
            tipoPregunta = p.tipoPregunta();
            switch (tipoPregunta) {
                case 1:
                    jListRptas.setVisible(false);
                    jTextAreaPregunta.setVisible(false);
                    jLabelRespuestas.setText("¿Verdadero o Falso?");
                    jRadioButtonArriba.setVisible(true);
                    jRadioButtonAbajo.setVisible(true);
                    jRadioButtonArriba.setText(p.getRespuestas().get(0).substring(1));
                    jRadioButtonAbajo.setText(p.getRespuestas().get(1).substring(1));
                    break;
                case 2:
                    jTextAreaPregunta.setVisible(false);
                    jRadioButtonArriba.setVisible(false);
                    jRadioButtonAbajo.setVisible(false);
                    cargarLista(p);
                    jLabelRespuestas.setText("Seleccione una/s respuesta/s de la lista");
                    break;
                case 3:
                    jListRptas.setVisible(false);
                    jRadioButtonArriba.setVisible(false);
                    jRadioButtonAbajo.setVisible(false);
                    jLabelRespuestas.setText("Ingrese su respuesta corta");
                    jTextAreaPregunta.setVisible(true);
                    break;
                default:
                    break;
            }
            jLabelPregunta.setText(p.getPregunta());
            jLabelPregNum.setText("Pregunta número: " + contadorPreg);
            tiempo = p.getTiempo();
        } else {
            estadistica.escribirEstadistica();
            estadistica.crearPdf();
            Resultado r = new Resultado(estadistica);
            r.setLocationRelativeTo(null);
            r.setVisible(true);
            //this.dispose();
        }
        TimerTask task = new TimerTask() {
            public void run() {
                
                jLabelTiempo.setText("Tiempo restante: "
                        + Integer.toString(tiempo));
                tiempo--;
                if (tiempo == -1) {
                    timer.cancel();
                } else if (start) {
                    timer.cancel();
                    start = false;
                }
            }
        };
    }//GEN-LAST:event_jButtonSIguienteActionPerformed

    private void jButtonComenzarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonComenzarActionPerformed
        
        p = modelo.getQuiz().getPreguntas().get(0);
        tipoPregunta = p.tipoPregunta();
        cantPreguntas = modelo.getQuiz().getPreguntas().size();
        jLabelPregunta.setText(p.getPregunta());
        jLabelPregNum.setText(jLabelPregNum.getText() + contadorPreg);
        jLabelTiempo.setText("Tiempo restante: " + p.getTiempo());
        tiempo = p.getTiempo();
        TimerTask task = new TimerTask() {
            public void run() {
                jLabelTiempo.setText("Tiempo restante: "
                        + Integer.toString(tiempo)); //the timer lable to counter.
                tiempo--;
                if (tiempo == -1) {
                    timer.cancel();
                } else if (start) {
                    timer.cancel();
                    start = false;
                }
            }
        };
        timer.scheduleAtFixedRate(task, 1000, 1000);
        switch (tipoPregunta) {
            case 2:
                cargarLista(p);
               
                jLabelRespuestas.setText("Seleccione una/s respuesta/s de la lista");
                jRadioButtonArriba.setVisible(false);
                jRadioButtonAbajo.setVisible(false);
                jRadioButtonArriba.setVisible(false);
                jRadioButtonAbajo.setVisible(false);
                break;
            case 1:
                jListRptas.setVisible(false);
                jTextAreaPregunta.setVisible(false);
                jLabelRespuestas.setText("¿Verdadero o Falso?");
                jRadioButtonArriba.setVisible(true);
                jRadioButtonAbajo.setVisible(true);
                jRadioButtonArriba.setText(p.getRespuestas().get(0).substring(1));
                jRadioButtonAbajo.setText(p.getRespuestas().get(1).substring(1));
                break;
            case 3:
                jListRptas.setVisible(false);
                jRadioButtonArriba.setVisible(false);
                jRadioButtonAbajo.setVisible(false);
                jLabelRespuestas.setText("Ingrese su respuesta corta");
                jTextAreaPregunta.setVisible(true);
                break;
            default:
                break;
        }
        jButtonComenzar.setVisible(false);

    }//GEN-LAST:event_jButtonComenzarActionPerformed

    private void jButtonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalirActionPerformed
        start = false;
        MenuPrincipal menu = new MenuPrincipal(modelo);
        menu.setLocationRelativeTo(null);
        menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButtonSalirActionPerformed

    private void jListRptasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListRptasMouseClicked
        jLabelGif.setVisible(false);
        jLabelComentarios.setVisible(false);
    }//GEN-LAST:event_jListRptasMouseClicked
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Juego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barraProgreso;
    private javax.swing.JButton jButtonComenzar;
    private javax.swing.JButton jButtonSIguiente;
    private javax.swing.JButton jButtonSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelComentarios;
    private javax.swing.JLabel jLabelGif;
    private javax.swing.JLabel jLabelPregNum;
    private javax.swing.JLabel jLabelPregunta;
    private javax.swing.JLabel jLabelRespuestas;
    private javax.swing.JLabel jLabelTiempo;
    private javax.swing.JList<String> jListRptas;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButtonAbajo;
    private javax.swing.JRadioButton jRadioButtonArriba;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextAreaPregunta;
    // End of variables declaration//GEN-END:variables
}
