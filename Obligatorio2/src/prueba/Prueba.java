package prueba;

import java.io.IOException;
import dominio.Sistema;
import interfaz.MenuPrincipal;

public class Prueba {

    public static void main(String[] args) throws IOException {
        Sistema sistema = new Sistema();
        MenuPrincipal menu = new MenuPrincipal(sistema);
        menu.setVisible(true);
        menu.setLocationRelativeTo(null);

    }
}
