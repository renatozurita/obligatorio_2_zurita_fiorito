package dominio;

import java.util.ArrayList;

public class Quiz {

    private String descripcion;
    private String direccion;
    private ArrayList<Pregunta> preguntas;
    private int cantidadPreguntas;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public ArrayList<Pregunta> getPreguntas() {
        return this.preguntas;
    }

    public void agregarPregunta(Pregunta p) {
        preguntas.add(p);
    }

    public Quiz(String direccion) {
        this.direccion = direccion;
    }

    public Quiz() {
        this.direccion = "";
        this.descripcion = "";
        this.preguntas = new ArrayList<Pregunta>();
        this.cantidadPreguntas = 0;
       
    }

    public int getCantidadPreguntas() {
        return cantidadPreguntas;
    }

    public void setCantidadPreguntas(int cantidadPreguntas) {
        this.cantidadPreguntas = cantidadPreguntas;
    }
    
      @Override
    public String toString() {
        return this.getDescripcion();
    }

}
