package dominio;

import java.util.ArrayList;
import java.io.FileOutputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Estadistica {
    private ArrayList<String> respuestasUsuario;
    private ArrayList<Pregunta> preguntas;
    private ArrayList<Integer> tiempos;
    private int cantidadPreguntas;
    String textoEstadistico = "";

    public Estadistica(){
        respuestasUsuario = new ArrayList<>();
        preguntas = new ArrayList<>();
        tiempos = new ArrayList<>();
    }
    
    public void escribirEstadistica(){
        String texto = "";
        for (int i = 0; i < preguntas.size(); i++) {
            Pregunta p = this.getPreguntas().get(i);
            Integer t = this.getTiempos().get(i);
            String textoPregunta = p.getPregunta();
            String textoRespuesta = this.getRespuestasUsuario().get(i);
            String correctaSinIgual = (p.correcta(p)).substring(1);
            texto+= "Pregunta " + (i+1) + ", " + textoPregunta + "\n"
                    + "Su respuesta fue: " + textoRespuesta + "\n"
                    + "Y la correcta era: " + correctaSinIgual + "\n"
                    + " Demoraste "+t+" segundos. \n";
        }
        double promedioTiempo = 0;
        for (int i = 0; i < tiempos.size(); i++) {
            promedioTiempo+=tiempos.get(i);
        }
        promedioTiempo=promedioTiempo/tiempos.size();
        DecimalFormat df = new DecimalFormat("#.##");
        String tiempoPromedio= (df.format(promedioTiempo));
        texto+=" Tu tiempo promedio fue de " + tiempoPromedio + " segundos.";
        this.setTextoEstadistico(texto);
    }
    
    public void crearPdf(){
        try{
        Document documento = new Document();
        DateFormat formatoFechaNombreArchivo = new SimpleDateFormat("ddMMyyyy HH.mm");
        DateFormat formatoFechaTexto = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat formatoHoraTexto = new SimpleDateFormat(" HH:mm:ss");
	Date fecha = new Date();
        String fechaArchivo = formatoFechaNombreArchivo.format(fecha);
        String fechaTexto = formatoFechaTexto.format(fecha);
        String horaTexto = formatoHoraTexto.format(fecha);
        PdfWriter.getInstance(documento,new FileOutputStream("./src/PDFs/Estadistica_"+fechaArchivo+".pdf"));
        Font fuenteTitulo = new Font(Font.FontFamily.HELVETICA,20,Font.BOLD);
        documento.open();
        Image imagen = Image.getInstance("./src/Imagenes/Resumen.png");
        documento.add(imagen);
        documento.add(new Paragraph("Reporte de la partida del " + fechaTexto + " a las"+horaTexto, fuenteTitulo));
        documento.addTitle("Reporte de la partida de EduQuizzes");
        documento.add(new Paragraph(this.getTextoEstadistico()));
        documento.close();
        }catch(Exception e){
            System.out.println(e);
        }
    }

    public ArrayList<String> getRespuestasUsuario() {
        return respuestasUsuario;
    }

    public void setRespuestasUsuario(ArrayList<String> respuestasUsuario) {
        this.respuestasUsuario = respuestasUsuario;
    }

    public ArrayList<Pregunta> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(ArrayList<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }

    public ArrayList<Integer> getTiempos() {
        return tiempos;
    }

    public void setTiempos(ArrayList<Integer> tiempos) {
        this.tiempos = tiempos;
    }

    public int getCantidadPreguntas() {
        return cantidadPreguntas;
    }

    public void setCantidadPreguntas(int cantidadPreguntas) {
        this.cantidadPreguntas = cantidadPreguntas;
    }

    public String getTextoEstadistico() {
        return textoEstadistico;
    }

    public void setTextoEstadistico(String textoEstadistico) {
        this.textoEstadistico = textoEstadistico;
    }
    
}
