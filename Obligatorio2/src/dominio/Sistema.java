package dominio;

import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;

public class Sistema {

    private ArrayList<Quiz> listaQuizzes;
    private ArrayList<String> listaNombres;
    String textoRetorno = "";
    Scanner scanner = new Scanner(System.in);
    Quiz q = new Quiz();
    Estadistica e = new Estadistica();

    public Estadistica getE() {
        return e;
    }

    public void setE(Estadistica e) {
        this.e = e;
    }

    public Quiz getQuiz() {
        return q;
    }
    
    public void setQuiz(Quiz q) {
        this.q = q;
    }
    
    public ArrayList<String> getListaNombres() {
        return listaNombres;
    }

    public void listarQuiz(Quiz unQuiz) {
        this.getListaQuizes().add(unQuiz);
    }

    public void listarNombre(String unNombre) {
        this.getListaNombres().add(unNombre);
    }

    public ArrayList<Quiz> getListaQuizes() {
        return listaQuizzes;
    }
    
     public Sistema() {
        listaQuizzes = new ArrayList<>();
        listaNombres = new ArrayList<>();
    }

    public void leerArchivo(String ruta) {
        String[] pregunta = null;
        ArrayList<String> rptas; //Lista de respuestas
        String txtPreg = "";
        String titlePreg = "";
        int cantidadPreguntas = 0;
        int tiempo = 30;
        try {
            String bfRead;
            BufferedReader br = new BufferedReader(new FileReader(ruta));
            while ((bfRead = br.readLine()) != null) {
                if (bfRead.isEmpty()) {
                    cantidadPreguntas++;
                } else if (bfRead.contains("::")) {
                    pregunta = bfRead.split("::");
                    txtPreg = pregunta[2].substring(0, pregunta[2].length() - 1);
                    titlePreg = pregunta[1];
                    if (txtPreg.startsWith("//")) {
                        tiempo = Integer.parseInt(txtPreg.substring(2, 4));
                        txtPreg = txtPreg.substring(4);
                    }
                    rptas = new ArrayList(); //Lista de respuestas
                    while (!(bfRead = br.readLine()).contains("}")) {
                        if (bfRead.startsWith("~") || bfRead.startsWith("=")) {
                            rptas.add(bfRead);
                        }
                    }
                    Pregunta p = new Pregunta(txtPreg, rptas);
                    p.setTiempo(tiempo);
                    q.agregarPregunta(p);
                }
            }
            listarQuiz(q);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Archivo seleccionado "
                    + "no es de formato GIFT.",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        q.setCantidadPreguntas(cantidadPreguntas);
        String nombre = nombreArchivo(ruta);
        listarNombre(nombre);
        e.setCantidadPreguntas(cantidadPreguntas);
        e.setPreguntas(q.getPreguntas());  
    }
    
    

    public Boolean hayProximaPregunta(Pregunta p) {
        int largoLista = q.getPreguntas().size();
        return q.getPreguntas().indexOf(p) < largoLista;
    }

    public Pregunta proximaPregunta(Pregunta p) {
        int indicePreg = q.getPreguntas().indexOf(p);
        if (indicePreg < q.getPreguntas().size()) {

            return q.getPreguntas().get(1 + indicePreg);
        } else {
            return p;
        }
    }

    public String nombreArchivo(String ruta) {
        int ultimaBarra = 0;
        for (int i = 0; i < ruta.length(); i++) {
            if (ruta.charAt(i) == '\\') {
                ultimaBarra = i;
            }
        }
        String nombre = "";
        for (int i = ultimaBarra + 1; i < ruta.length(); i++) {
            nombre += ruta.charAt(i);
        }
        return nombre;
    }
}
