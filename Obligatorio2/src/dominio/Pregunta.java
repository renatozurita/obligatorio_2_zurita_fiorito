package dominio;

import java.util.ArrayList;

public class Pregunta {

    private String pregunta;
    private String titulo;
    private ArrayList<String> respuestas;
    private int numeroPregunta;
    private int tiempo;

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }
    
    public int getTiempo() {
        return tiempo;
    }
    
    public void setTiempo (int tiempo) {
        this.tiempo = tiempo;
    }

    public ArrayList<String> getRespuestas() {
        return respuestas;
    }

    public void agregarRespuesta(String r) {
        this.respuestas.add(r);
    }

    public Pregunta(String pregunta) {
        this.pregunta = pregunta;
        this.respuestas = new ArrayList<String>();
        this.numeroPregunta = 0;
        this.tiempo = 30;
    }

    public Pregunta(String pregunta, ArrayList<String> respuestas) {
        this.pregunta = pregunta;
        this.respuestas = respuestas;

    }

    public Pregunta() {
        this.pregunta = "";
        this.respuestas = new ArrayList<String>();
        this.tiempo = 30;
    }

    //Devuelve la respuesta correcta de cada pregunta
    public String correcta(Pregunta p) {
        String correcta = "";
        for (int i = 0; i < p.getRespuestas().size(); i++) {
            if (p.getRespuestas().get(i).startsWith("=")) {
                correcta = p.getRespuestas().get(i);
            }
        }
        return correcta;
    }
    

    //tipoPreg devuelve 1 = V o F, 2 = Multiple opción, 3 = repta corta
    public Integer tipoPregunta() {
        int ret = 0;
        if (this.getRespuestas().size() == 1) { //si la respuesta es única, entonces es respuesta corta
            ret = 3;
        } else {            
                if (this.getRespuestas().size() == 2 && 
                        (this.getRespuestas().get(0).contains("True")
                        || this.getRespuestas().get(0).contains("False")
                        || this.getRespuestas().get(0).contains("T")
                        || this.getRespuestas().get(0).contains("F"))) {
                    ret = 1;
                } else {
                    ret = 2;
                }
        }
        return ret;
    }
}
