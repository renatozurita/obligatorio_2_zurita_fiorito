package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class QuizTest {
    
    public QuizTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testGetDescripcion() {
        System.out.println("getDescripcion");
        String descr = "descripcion";
        Quiz q = new Quiz();
        q.setDescripcion("descripcion");
        String exp = q.getDescripcion();
        assertEquals(descr, exp);        
    }
    
    @Test 
    public void testSetDescripcion() {
        System.out.println("setDescripcion");
        String descr = "descripcion";
        Quiz q = new Quiz();
        q.setDescripcion(descr);
        assertEquals(descr, q.getDescripcion());
    }
    
    @Test
    public void testSetDireccion() {
        System.out.println("setDireccion");
        String dir = "direccion";
        Quiz q = new Quiz();
        q.setDireccion(dir);
        assertEquals(dir, q.getDireccion());
    }
    
    @Test
    public void testGetDireccion() {
        System.out.println("getDireccion");
        String dir = "direccion";
        Quiz q = new Quiz();
        q.setDireccion(dir);
        String exp = q.getDireccion();
        assertEquals(dir, exp);
    }
    
    @Test
    public void testGetPreguntas() {
        System.out.println("getPreguntas");
        Quiz q = new Quiz();
        ArrayList<Pregunta> pregs = new ArrayList<>();
        ArrayList<Pregunta> exp = q.getPreguntas();
        assertEquals(pregs, exp);        
    }
    
    @Test
    public void testAgregarPregunta() {
        System.out.println("agregarPregunta");
        Quiz q = new Quiz();
        Pregunta p = new Pregunta();
        p.setPregunta("pregunta");
        String preg = "pregunta";
        q.agregarPregunta(p);
        assertEquals(preg, q.getPreguntas().get(0).getPregunta());
    }
    
    @Test
    public void testGetCantidadPreguntas() {
        System.out.println("getCantidadPreguntas");
        Quiz q = new Quiz();
        int cant = 3;
        q.setCantidadPreguntas(cant);
        int exp = q.getCantidadPreguntas();
        assertEquals(exp, cant);
    }
    
    @Test
    public void testSetCantidadPreguntas() {
        System.out.println("setCantidadPreguntas");
        Quiz q = new Quiz();
        int cant = 3;
        q.setCantidadPreguntas(cant);
        assertEquals(q.getCantidadPreguntas(), cant);        
    }
}
