
package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class SistemaTest {
    
    public SistemaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetE() {
        System.out.println("getE");
        Sistema s = new Sistema();
        Estadistica e = new Estadistica();
        s.setE(e);
        Estadistica exp = s.getE();
        assertEquals(s.getE(), e);        
    }
    
    @Test
    public void testSetE() {
        System.out.println("setE");
        Sistema s = new Sistema();
        Estadistica e = new Estadistica();
        s.setE(e);
        assertEquals(e, s.getE());
        
    }
    
     @Test
     public void testGetQuiz() {
         System.out.println("getQuiz");
         Quiz q = new Quiz();
         Sistema s = new Sistema();
         s.setQuiz(q);
         assertEquals(q, s.getQuiz());
     }
     
     @Test
     public void testSetQuiz() {
         System.out.println("setQuiz");
         Quiz q = new Quiz();
         Sistema s = new Sistema();
         s.setQuiz(q);
         Quiz exp = s.getQuiz();
         assertEquals(exp, q);
     }
     
     @Test
     public void testGetListaNombres(){
         System.out.println("getListaNombres");
         Sistema s = new Sistema();
         ArrayList<String> nombres = new ArrayList<>();
         ArrayList<String> exp = s.getListaNombres();
         assertEquals(exp, nombres);         
     }
     
     @Test
     public void testListarQuiz() {
         System.out.println("listarQuiz");
         Sistema s = new Sistema();
         Quiz q = new Quiz();
         s.listarQuiz(q);
         Quiz exp = s.getListaQuizes().get(0);
         assertEquals(exp, q);
     }
     
     @Test
     public void testListarNombre() {
         System.out.println("listarNombre");
         Sistema s = new Sistema();
         String nombre = "nombre";
         s.listarNombre(nombre);
         String exp = s.getListaNombres().get(0);
         assertEquals(exp, nombre);
     }
     
     @Test
     public void testGetListaQuizes() { 
         System.out.println("getListaQuizes");
         Sistema s = new Sistema();
         ArrayList<Quiz> q = new ArrayList();
         ArrayList<Quiz> exp = s.getListaQuizes();
         assertEquals(q,exp);
         
     }
     
     @Test
     public void testHayProximaPregunta(){
         System.out.println("hayProximaPregunta");
         Sistema s = new Sistema();
         Pregunta p1 = new Pregunta();
         Pregunta p2 = new Pregunta();
         s.q.agregarPregunta(p1);
         s.q.agregarPregunta(p2);
         boolean res = s.hayProximaPregunta(p1);
         boolean exp = true;
         assertEquals(res, exp);
     }
     
     @Test
     public void testProximaPregunta(){
         System.out.println("proximaPregunta");
         Sistema s = new Sistema();
         Pregunta p1 = new Pregunta();
         Pregunta p2 = new Pregunta();
         s.q.agregarPregunta(p1);
         s.q.agregarPregunta(p2);
         Pregunta p = s.proximaPregunta(p1);
         assertEquals(p, p2);         
     }
     
     
}
