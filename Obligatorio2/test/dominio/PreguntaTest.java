package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class PreguntaTest {
    
    public PreguntaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testGetPregunta()   {
        System.out.println("getPregunta");
        String pregunta = "Test getPregunta";        
        Pregunta p = new Pregunta();
        p.setPregunta("Test getPregunta");  
        String exp = p.getPregunta();
        assertEquals(pregunta, exp);
    }
    
    @Test
    public void testSetPregunta() {
        System.out.println("setPregunta");
        String preg = "Test setPregunta";
        Pregunta p = new Pregunta();
        p.setPregunta(preg);
        assertEquals(preg, p.getPregunta());        
    }
    
    @Test
    public void testGetTiempo() {
        System.out.println("getTiempo");
        int tiempo = 30;
        Pregunta p = new Pregunta();
        p.setTiempo(30);
        int exp = p.getTiempo();
        assertEquals(tiempo, exp);
    }
    
    @Test
    public void testSetTiempo() {
        System.out.println("setTiempo");
        int tiempo = 30;
        Pregunta p = new Pregunta();
        p.setTiempo(tiempo);
        assertEquals(tiempo, p.getTiempo());
    }
    
    @Test
    public void testGetRespuestas() {
        System.out.println("testGetRespuestas");
        Pregunta p = new Pregunta();
        ArrayList<String> rptas = new ArrayList();
        ArrayList<String> result = p.getRespuestas();
        assertEquals(rptas, result);
    }
    
    @Test
    public void testAgregarRespuesta() {
        System.out.println("agregarRespuesta");
        String rpta = "respuesta";
        Pregunta p = new Pregunta();
        p.agregarRespuesta(rpta);
        assertEquals(p.getRespuestas().get(0), rpta);
    }
    
    @Test 
    public void testCorrecta() {
        System.out.println("correcta");
        Pregunta p = new Pregunta();
        p.setPregunta("Test correcta");
        p.agregarRespuesta("=esta");
        String exp = "=esta";
        assertEquals(exp, p.getRespuestas().get(0));       
    }
    
    @Test
    public void testTipoPregunta() {
        System.out.println("tipoPregunta");
        Pregunta p = new Pregunta();
        String resp = "T";
        p.agregarRespuesta(resp);
        int exp = 3;
        int test = p.tipoPregunta();
        assertEquals(test, exp);        
    }
     
}

